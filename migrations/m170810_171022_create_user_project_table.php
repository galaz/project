<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_project`.
 */
class m170810_171022_create_user_project_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user_project', [
            'id' => $this->primaryKey(),
			'userid' => $this->integer(),
			'projectid' => $this->integer(),
			'manager' => $this->boolean(),
			
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user_project');
    }
}
