<?php

use yii\db\Migration;

/**
 * Handles the creation of table `task`.
 */
class m170810_170936_create_task_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('task', [
            'id' => $this->primaryKey(),
			'status' => $this->string(),
			'title' => $this->string(),
			'notes' => $this->text(),
			'projectid' => $this->integer(),
			'end_date_required' => $this->date(),
			'end_date_actual' => $this->date(),
			'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
			
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('task');
    }
}
