<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_task`.
 */
class m170810_171010_create_user_task_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user_task', [
            'id' => $this->primaryKey(),
			'userid' => $this->integer(),
			'taskid' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user_task');
    }
}
