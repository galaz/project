<?php
namespace app\commands;

use Yii;
use yii\console\Controller;


class AddruleController extends Controller
{

	public function actionOwnUpdate()
	{	
		$auth = Yii::$app->authManager;	
		$rule = new \app\rbac\OwnUpdateRule;
		$auth->add($rule);
	}
}