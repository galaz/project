<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Project;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\UserProject */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-project-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'userid')->dropDownList(User::getUsers()) ?>

    <?= $form->field($model, 'projectid')->dropDownList(Project::getProjects()) ?>

	<?= $form->field($model, 'manager')->radioList([1 => 'Yes', 0 => 'No'])->label('Is Manager ?'); ?>  

  
  <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
   </div>

    <?php ActiveForm::end(); ?>

</div>
