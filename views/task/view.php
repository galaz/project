<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Task */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Tasks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'status',
            'title',
            'notes:ntext',
            'projectid',
			'end_date_required',  
            'end_date_actual',
			[ //  created by
				'label' => $model->attributeLabels()['created_by'],
				'value' => isset($model->createdBy->username) ? $model->createdBy->username : 'No one!',	
			],
			[ //  created at
				'label' => $model->attributeLabels()['created_at'],
				'value' => date('d/m/Y H:i:s', $model->created_at)
			],				
			[ //  updated by
				'label' => $model->attributeLabels()['updated_by'],
				'value' => isset($model->updateddBy->username) ? $model->updateddBy->username : 'No one!',	
			],
			[ // updated at
				'label' => $model->attributeLabels()['updated_at'],
				'value' => date('d/m/Y H:i:s', $model->updated_at)
			],	
        ],
    ]) ?>

</div>
