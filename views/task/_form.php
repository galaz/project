<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Status;
use app\models\Project;


/* @var $this yii\web\View */
/* @var $model app\models\Task */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-form">
	<?php if((Yii::$app->user->can('updateTask')&&(!(Yii::$app->user->can('resTask')))) : ?>

	<?php $form = ActiveForm::begin(); ?>
	
	<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'status')->
				dropDownList(Status::getStatuses()) ?>

    <?= $form->field($model, 'notes')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'projectid')->dropDownList(Project::getProjects()) ?>

    <?= $form->field($model, 'end_date_required')->textInput() ?>

    <?= $form->field($model, 'end_date_actual')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
	<?= else :?>
	
	 <?= $form->field($model, 'status')->
				dropDownList(Status::getStatuses()) ?>
				
	<div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>	
<?php endif; ?>	
				

</div>
