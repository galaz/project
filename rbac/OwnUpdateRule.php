<?php
namespace app\rbac;
use app\models\UserTask;
use app\models\Tak;
use yii\rbac\Rule;
use Yii; 
use yii\db\ActiveRecord;

class OwnUpdateRule extends Rule
{
	public $name = 'ownUpdateRule';

	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) {
			if(isset($_GET['id'])){
			    $checkUser = UserTask::findOne($user);
			    $checkTask = Task::findOne($checkUser);   
			    if(($checkUser->userid == $_GET['id'])&&($checkUser->taskid ==$checkTask->id))
				
			        return true;
			}
		}
		return false;
	}
}