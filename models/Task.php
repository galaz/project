<?php

namespace app\models;
Use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use Yii;

/**
 * This is the model class for table "task".
 *
 * @property integer $id
 * @property string $status
 * @property string $title
 * @property string $notes
 * @property integer $projectid
 * @property string $end_date_required
 * @property string $end_date_actual
 */
class Task extends \yii\db\ActiveRecord
{
	
	public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }
		public static function getTasks()
	{
		$allTasks = self::find()->all();
		$allTasksArray = ArrayHelper::
					map($allTasks, 'id', 'title');
		return $allTasksArray;						
	}
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notes'], 'string'],
            [['projectid','created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['end_date_required', 'end_date_actual'], 'safe'],
            [['status', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
	 
	 
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'title' => 'Title',
            'notes' => 'Notes',
            'projectid' => 'Project',
            'end_date_required' => 'End Date Required',
            'end_date_actual' => 'End Date Actual',
			'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',	
        ];
    }
	
	
	public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }	

	public function getUpdateddBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }	
  /**  public function getStatusItem()
    {
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }*/
	
}
