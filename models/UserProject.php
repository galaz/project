<?php

namespace app\models;
use yii\db\ActiveRecord;

use Yii;

/**
 * This is the model class for table "user_project".
 *
 * @property integer $id
 * @property integer $userid
 * @property integer $projectid
 * @property integer $manager
 */
class UserProject extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userid', 'projectid', 'manager'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userid' => 'User',
            'projectid' => 'Project',
            'manager' => 'Is Manager?',
        ];
    }
}
